using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CoinController : MonoBehaviour
{
    public float CoinSpeed;
    public float Value;

    public static UnityEvent Action;

    private void Awake()
    {
        if (Action == null)
        {
            Action = new UnityEvent();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, 0, CoinSpeed));
    }

    private void OnTriggerEnter(Collider other)
    {
        Action.Invoke();
        Destroy(gameObject);
    }
}
