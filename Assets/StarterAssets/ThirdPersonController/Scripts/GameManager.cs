using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int MaxCoins;
    private int _currentCoins;

    // Start is called before the first frame update
    void Start()
    {
        _currentCoins = 0;
        CoinController.Action.AddListener(AddCoin);

        if (MaxCoins == 0) MaxCoins = FindObjectsOfType<CoinController>().Length;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Restart()
    {
        SceneManager.LoadScene("Playground");
    }

    public void AddCoin()
    {
        _currentCoins++;
        if (MaxCoins == _currentCoins) Restart();
    }
}
